Feature: Homework
  Doing the homework

	@homework
  Scenario: Homework number one
  	Given I open the My Store page
	  	When I click on the Sign in button
	  	Then I redirect to the login page
	  	When I write "gazdag@gs.com" into signup field
	  	And I click on the Create an account button
	  	Then I redirect to the Create page
	  	When I click on Mr
	  	And I write "Laci" to my First Name
			And I write "Kis" to my Last Name
			And I write "12345" to my Password
			And I write "bb 6" to my Adress Adress
			And I write "belaapatfalva" to my Adress City
			And I select "Alabama" as My State
			And I write "67261" to my Adress Zip Code
			And I select "United States" as my Country
			And I write "0623432122" to my Moblie Phone
			Then I check if evertyhing is correct
	
	
	@homework
	Scenario: Homework number two
		Given I open the My Store page
		When I click on "Women" tab
		And I select the "Printed Dress" product
		Then I see the Quantity number is "1"
		When I click on Add to cart button
		And I click on continue shopping
		When I click on "Women" tab
		And I select the "Printed Summer Dress" product
		Then I see the Quantity number is "1"
		When I click on Add to cart button
		And I click on continue shopping
		When I click on "T-Shirts" tab
		And I select the "Faded Short Sleeve T-shirts" product
		And I set Quantity number to "3"
		Then I see the Quantity number is "3"
		When I click on Add to cart button
		And I click on continue shopping
		And I click on Proceed To Checkout
		Then I see the shopping cart page
		And I check total price
		