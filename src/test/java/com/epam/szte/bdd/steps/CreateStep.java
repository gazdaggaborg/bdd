package com.epam.szte.bdd.steps;

import org.junit.Assert;

import com.epam.szte.bdd.hooks.Hooks;
import com.epam.szte.bdd.pages.CreatePage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateStep {
	
	private CreatePage createPage = new CreatePage(Hooks.driver);
	
	
	@Then("I redirect to the Create page")
	public void redirectedToCreatePage() {
		Assert.assertTrue("The login form hasn't appeared", createPage.checkCreateForm());
	}
	
	@When("I click on Mr")
	public void clickOnMR() {
		createPage.clickOnMr();
	}
	
	@And("^I write \\\"([^\\\"]*)\\\" to my First Name$")
	public void writeFirstName(String firstName){
		createPage.writeFirstName(firstName);
	}
	
	@And("^I write \\\"([^\\\"]*)\\\" to my Last Name$")
	public void writeLastName(String lastName){
		createPage.writeLastName(lastName);
	}
	
	@And("^I write (.+) to my Email$")
	public void writeEmail(String emaill){
		createPage.writeEmail(emaill);
	}
	
	@And("^I write \\\"([^\\\"]*)\\\" to my Password$")
	public void writePassword(String password){
		createPage.writePassword(password);
	}
	
	@And("^I write \\\"([^\\\"]*)\\\" to my Adress First Name$")
	public void writeAdressFirstName(String firstName){
		createPage.writeAdressFirstName(firstName);
	}
	
	@And("^I write \\\"([^\\\"]*)\\\" to my Adress Last Name$")
	public void writeAdressLastName(String lastName){
		createPage.writeAdressLastName(lastName);
	}
	
	@And("^I write \\\"([^\\\"]*)\\\" to my Adress Adress$")
	public void writeAdressAdress(String adress){
		createPage.writeAdressAdress(adress);
	}
	
	@And("^I write \\\"([^\\\"]*)\\\" to my Adress City$")
	public void writeAdressCity(String city){
		createPage.writeAdressCity(city);
	}
	
	@And("^I select \\\"([^\\\"]*)\\\" as My State$")
	public void selectState(String state){
		createPage.selectState(state);
	}
	
	@And("^I write \\\"([^\\\"]*)\\\" to my Adress Zip Code$")
	public void writeAdressZip(String zipcode){
		createPage.writeAdressZip(zipcode);
	}
	
	@And("^I select \\\"([^\\\"]*)\\\" as my Country$")
	public void selectCountry(String country){
		createPage.selectCountry(country);
	}
	
	@And("^I write \\\"([^\\\"]*)\\\" to my Moblie Phone$")
	public void writeMobile(String mobile){
		createPage.writeMobile(mobile);
	}
	@Then("I check if evertyhing is correct")
	public void checkall() {
		createPage.checkEverything();
	}
	
}
