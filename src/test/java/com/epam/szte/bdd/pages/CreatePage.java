package com.epam.szte.bdd.pages;

import org.openqa.selenium.By;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.epam.szte.bdd.utils.PageObject;
import org.junit.Assert;

public class CreatePage extends PageObject {

	private WebDriver driver;
	
	public CreatePage(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	@FindBy(id="submitAccount")
	private WebElement createButton;
	
	public boolean checkCreateForm() {
		return createButton.isDisplayed();
	}
	
	@FindBy(id="id_gender1")
	private WebElement genderMr;
	
	public void clickOnMr(){
		 genderMr.click();
	}
	
	@FindBy(id="customer_firstname")
	private WebElement firstNameField;
	
	public void writeFirstName(String firstName) {
		firstNameField.sendKeys(firstName);
		
	}

	@FindBy(id="customer_lastname")
	private WebElement lastNameField;
	
	public void writeLastName(String lastName) {
		lastNameField.sendKeys(lastName);
		
	}
	
	@FindBy(id="email")
	private WebElement emailField;
	
	public void writeEmail(String email) {
		emailField.sendKeys(email);
		
	}

	@FindBy(id="passwd")
	private WebElement passField;
	
	public void writePassword(String password) {
		passField.sendKeys(password);
		
	}
	
	@FindBy(id="firstname")
	private WebElement AdressfirstNameField;
	
	public void writeAdressFirstName(String firstName) {
		AdressfirstNameField.sendKeys(firstName);
		
	}

	@FindBy(id="lastname")
	private WebElement AdressLastNameField;
	
	public void writeAdressLastName(String lastName) {
		AdressLastNameField.sendKeys(lastName);
	}
	
	@FindBy(id="address1")
	private WebElement AdressField;
	
	public void writeAdressAdress(String adress) {
		AdressField.sendKeys(adress);
	}
	
	@FindBy(id="city")
	private WebElement CityField;
	
	public void writeAdressCity(String city) {
		CityField.sendKeys(city);
	}
	
	@FindBy(id="id_state")
	private WebElement StateField;
	
	public void selectState(String state) {
    	Select dropdown = new Select(StateField);
    	dropdown.selectByVisibleText(state);
	}
	
	@FindBy(id="postcode")
	private WebElement ZipField;
	
	public void writeAdressZip(String zipCode) {
		ZipField.sendKeys(zipCode);
	}
	
	
	@FindBy(id="id_country")
	private WebElement CountryField;
	
	public void selectCountry(String Country) {
    	Select dropdown = new Select(CountryField);
    	dropdown.selectByVisibleText(Country);
	}
	
	@FindBy(id="phone_mobile")
	private WebElement MobileField;
	
	public void writeMobile(String mobilenumber) {
		MobileField.sendKeys(mobilenumber);
	}

	@FindBy(id="email")
	private WebElement emailCheck;
	
	public void checkEverything() {
		
		Select dropdown = new Select(StateField);
		Select dropdowntwo = new Select(CountryField);
		
		Assert.assertEquals("Laci", firstNameField.getAttribute("value"));
		Assert.assertEquals("Kis", lastNameField.getAttribute("value"));
		Assert.assertEquals("gazdag@gs.com", emailField.getAttribute("value"));
		Assert.assertEquals("bb 6", AdressField.getAttribute("value"));
		Assert.assertEquals("belaapatfalva", CityField.getAttribute("value"));
	
		Assert.assertEquals("67261", ZipField.getAttribute("value"));
		
		Assert.assertEquals("0623432122", MobileField.getAttribute("value"));
		Assert.assertEquals("Laci", AdressfirstNameField.getAttribute("value"));
		Assert.assertEquals("Kis", AdressLastNameField.getAttribute("value"));
		
		Assert.assertEquals("Alabama", dropdown.getFirstSelectedOption().getText());
		Assert.assertEquals("United States", dropdowntwo.getFirstSelectedOption().getText());
	}
	
}
